from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import sys
import glob

filepath = []
fileCode = []

def giveFileNames():
    for file in glob.glob("C:\\Users\\wojte_000\\Desktop\\pracadomowa2\\pracadomowa3\\zaliczonko\\venv\\pliki\\*.py"):
        filepath.append(file)

def readFile(a):
    fileCode.clear()
    f = open(filepath[a], "r")
    lines = f.readlines()
    for i in range(0, len(lines)):
        fileCode.append(lines[i])
    f.close()

def addToWebsite():
    driver = webdriver.Chrome()
    driver.get("https://pastebin.com")
    assert "Pastebin" in driver.title
    elem = driver.find_element_by_name("paste_code")
    elem.clear()
    i = 0
    for i in range(len(fileCode)):
        elem.send_keys(fileCode[i])
    elem = driver.find_element_by_name("submit")
    elem.click()
    time.sleep(2)
    newUrl = driver.current_url
    fl = open("LinkiDoPastebina.txt", "w+")
    fl.write(newUrl + "\n")
    driver.close()

def postAll():
    giveFileNames()
    for i in range(0, len(filepath)):
        readFile(i)
        addToWebsite()


postAll()