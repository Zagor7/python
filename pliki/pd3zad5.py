class Animals:
    def __init__(self, weight):
        self.weight = weight

    def __eq__(self, other):
        return self.weight == other.weight

    def __lt__(self, other):
        return self.weight < other.weight

    def __gt__(self, other):
        return self.weight > other.weight

    def __le__(self, other):
        return self.weight <= other.weight

    def __ge__(self, other):
        return self.weight >= other.weight

    def __ne__(self, other):
        return self.weight != other.weight


class Reptiles(Animals):
    pass


class Mammals(Animals):
    pass


class WaterReptiles(Reptiles):
    pass


class LandReptiles(Reptiles):
    pass


class Chameleon(LandReptiles):
    pass



#testchameleon = Chameleon(40)
#testchameleon2 = Chameleon(50)
#testprint(chameleon > chameleon2)